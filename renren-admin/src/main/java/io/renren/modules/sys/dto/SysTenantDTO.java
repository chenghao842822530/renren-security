package io.renren.modules.sys.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 租户表
 *
 * @author ch 842822530@qq.com
 * @since 1.0.0 2023-02-20
 */
@Data
@ApiModel(value = "租户表")
public class SysTenantDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "主键")
	private Long id;

	@ApiModelProperty(value = "租户ID")
	private Long tenantId;

	@ApiModelProperty(value = "租户名称")
	private String tenantName;

	@ApiModelProperty(value = "域名地址")
	private String domain;

	@ApiModelProperty(value = "系统背景")
	private String backgroundUrl;

	@ApiModelProperty(value = "联系人")
	private String linkman;

	@ApiModelProperty(value = "联系电话")
	private String contactNumber;

	@ApiModelProperty(value = "联系地址")
	private String address;

	@ApiModelProperty(value = "账号额度")
	private Integer accountNumber;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd" , timezone = "GMT+8")
	@ApiModelProperty(value = "过期时间")
	private Date expireTime;

	@ApiModelProperty(value = "数据源ID")
	private Long datasourceId;

	@ApiModelProperty(value = "授权码")
	private String licenseKey;

	@ApiModelProperty(value = "状态：0：启用 1：禁用")
	private Integer status;

	@ApiModelProperty(value = "创建部门")
	private Long depId;

	@ApiModelProperty(value = "创建人")
	private Long creator;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;

	@ApiModelProperty(value = "修改人")
	private Long updater;

	@ApiModelProperty(value = "修改时间")
	private Date updateDate;


}