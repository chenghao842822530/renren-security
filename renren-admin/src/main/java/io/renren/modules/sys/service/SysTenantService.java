package io.renren.modules.sys.service;

import io.renren.common.service.CrudService;
import io.renren.modules.sys.dto.SysTenantDTO;
import io.renren.modules.sys.entity.SysTenantEntity;

/**
 * 租户表
 *
 * @author ch 842822530@qq.com
 * @since 1.0.0 2023-02-20
 */
public interface SysTenantService extends CrudService<SysTenantEntity, SysTenantDTO> {

}