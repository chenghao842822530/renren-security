package io.renren.modules.sys.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.renren.common.entity.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 租户表
 *
 * @author ch 842822530@qq.com
 * @since 1.0.0 2023-02-20
 */
@Data
@TableName("sys_tenant")
public class SysTenantEntity implements Serializable {

    /**
     * id
     */
    @TableId
    private Long id;
    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private Long  creator;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createDate;

    /**
     * 租户ID
     */
	private Long tenantId;
    /**
     * 租户名称
     */
	private String tenantName;
    /**
     * 域名地址
     */
	private String domain;
    /**
     * 系统背景
     */
	private String backgroundUrl;
    /**
     * 联系人
     */
	private String linkman;
    /**
     * 联系电话
     */
	private String contactNumber;
    /**
     * 联系地址
     */
	private String address;
    /**
     * 账号额度
     */
	private Integer accountNumber;
    /**
     * 过期时间
     */
	private Date expireTime;
    /**
     * 数据源ID
     */
	private Long datasourceId;
    /**
     * 授权码
     */
	private String licenseKey;
    /**
     * 状态：0：启用 1：禁用
     */
	private Integer status;
    /**
     * 创建部门
     */
	private Long depId;
    /**
     * 修改人
     */
	private Long updater;
    /**
     * 修改时间
     */
	private Date updateDate;
}