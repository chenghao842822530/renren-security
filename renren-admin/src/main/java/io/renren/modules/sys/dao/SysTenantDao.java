package io.renren.modules.sys.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.sys.entity.SysTenantEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 租户表
 *
 * @author ch 842822530@qq.com
 * @since 1.0.0 2023-02-20
 */
@Mapper
public interface SysTenantDao extends BaseDao<SysTenantEntity> {
	
}