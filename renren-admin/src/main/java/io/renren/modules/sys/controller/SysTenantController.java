package io.renren.modules.sys.controller;

import cn.hutool.core.util.ArrayUtil;
import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.sys.dto.SysRoleDTO;
import io.renren.modules.sys.dto.SysTenantDTO;
import io.renren.modules.sys.excel.SysTenantExcel;
import io.renren.modules.sys.service.SysTenantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 租户表
 *
 * @author ch 842822530@qq.com
 * @since 1.0.0 2023-02-20
 */
@RestController
@RequestMapping("sys/tenant")
@Api(tags="租户表")
public class SysTenantController {
    @Autowired
    private SysTenantService sysTenantService;

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("sys:tenant:page")
    public Result<PageData<SysTenantDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
        PageData<SysTenantDTO> page = sysTenantService.page(params);

        return new Result<PageData<SysTenantDTO>>().ok(page);
    }

    @GetMapping("list")
    @ApiOperation("列表")
    @RequiresPermissions("sys:tenant:list")
    public Result<List<SysTenantDTO>> list(){
        List<SysTenantDTO> data = sysTenantService.list(new HashMap<>(1));

        return new Result<List<SysTenantDTO>>().ok(data);
    }


    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("sys:tenant:info")
    public Result<SysTenantDTO> get(@PathVariable("id") Long id){
        SysTenantDTO data = sysTenantService.get(id);

        return new Result<SysTenantDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("sys:tenant:save")
    public Result save(@RequestBody SysTenantDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        sysTenantService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("sys:tenant:update")
    public Result update(@RequestBody SysTenantDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        sysTenantService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("sys:tenant:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");
        //平台方数据属于内置，不可删除
        boolean contains = ArrayUtil.contains(ids, 1L);
        if (contains) {
            return new Result().error("平台方不可以删除！");
        }

        sysTenantService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("sys:tenant:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<SysTenantDTO> list = sysTenantService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, SysTenantExcel.class);
    }

}