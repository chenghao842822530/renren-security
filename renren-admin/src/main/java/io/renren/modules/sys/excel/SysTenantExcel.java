package io.renren.modules.sys.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 租户表
 *
 * @author ch 842822530@qq.com
 * @since 1.0.0 2023-02-20
 */
@Data
public class SysTenantExcel {
    @Excel(name = "主键")
    private Long id;
    @Excel(name = "租户ID")
    private String tenantId;
    @Excel(name = "租户名称")
    private String tenantName;
    @Excel(name = "域名地址")
    private String domain;
    @Excel(name = "系统背景")
    private String backgroundUrl;
    @Excel(name = "联系人")
    private String linkman;
    @Excel(name = "联系电话")
    private String contactNumber;
    @Excel(name = "联系地址")
    private String address;
    @Excel(name = "账号额度")
    private Integer accountNumber;
    @Excel(name = "过期时间")
    private Date expireTime;
    @Excel(name = "数据源ID")
    private Long datasourceId;
    @Excel(name = "授权码")
    private String licenseKey;
    @Excel(name = "状态：0：启用 1：禁用")
    private Integer status;
    @Excel(name = "创建部门")
    private Long depId;
    @Excel(name = "创建人")
    private Long creator;
    @Excel(name = "创建时间")
    private Date createDate;
    @Excel(name = "修改人")
    private Long updater;
    @Excel(name = "修改时间")
    private Date updateDate;

}