package io.renren.common.utils;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import org.springframework.util.Assert;

import java.util.Random;

public class StringUtil {

    /**
     * 随机数生成
     *
     * @param count      字符长度
     * @param randomType 随机数类别
     * @return 随机数
     */
    public static String random(int count, RandomType randomType) {
        if (count == 0) {
            return StringPool.EMPTY;
        }
        Assert.isTrue(count > 0, "Requested random string length " + count + " is less than 0.");
        final Random random = Holder.SECURE_RANDOM;
        char[] buffer = new char[count];
        for (int i = 0; i < count; i++) {
            String factor = randomType.getFactor();
            buffer[i] = factor.charAt(random.nextInt(factor.length()));
        }
        return new String(buffer);
    }
}
